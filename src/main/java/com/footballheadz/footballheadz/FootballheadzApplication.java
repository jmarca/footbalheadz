package com.footballheadz.footballheadz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FootballheadzApplication {

    public static void main(String[] args) {
        SpringApplication.run(FootballheadzApplication.class, args);
    }

}
